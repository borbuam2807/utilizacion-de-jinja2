from flask import Flask, render_template, request, redirect
from pymongo import MongoClient

app = Flask(__name__)

# Establecer la conexión a la base de datos MongoDB
client = MongoClient('mongodb://localhost:27017/')
db = client['slang_panameno']
collection = db['slang']

@app.route('/')
def inicio():
    return "Bienvenido al Diccionario de Slang Panameño"

@app.route('/agregar-palabra', methods=['GET', 'POST'])
def formulario_agregar():
    if request.method == 'POST':
        palabra = request.form['palabra']
        significado = request.form['significado']
        nueva_palabra = {"palabra": palabra, "significado": significado}
        collection.insert_one(nueva_palabra)
        return redirect('/agregar-palabra')
    return render_template('formulario_agregar.html')

# Otras rutas y funciones aquí, como editar, eliminar, ver listado, buscar significado, etc.

if __name__ == '__main__':
    app.run(debug=True)
